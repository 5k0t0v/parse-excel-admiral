SET AUTO_INCREMENT_INCREMENT = 1;
CREATE TABLE `employees`
(
    `id`   int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `name` varchar(255)    NOT NULL
);

CREATE TABLE `intervals`
(
    `id`           INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
#     `date`         date            NOT NULL,
    `employee_id`  INT             NOT NULL,
    `access_point` ENUM (
        'C920',
        'SEVERNY_POLUS',
        'PAPANIN',
        'MASLAK',
        'VDOVICHENKO',
        'PROLET5_CEH12')           NOT NULL,
    `start_time`   DATETIME,
    `end_time`     DATETIME,
    `duration`     INT
);
