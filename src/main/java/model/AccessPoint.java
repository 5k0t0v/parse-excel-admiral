package model;

public enum AccessPoint {
    C920,
    SEVERNY_POLUS,
    PAPANIN,
    MASLAK,
    VDOVICHENKO,
    PROLET5_CEH12;

    public static AccessPoint getAccessPoint(String name) {
        if (name.contains("С-920")) {
            return AccessPoint.C920;
        }
        if (name.contains("Северный полюс")) {
            return AccessPoint.SEVERNY_POLUS;
        }
        if (name.contains("Папанин")) {
            return AccessPoint.PAPANIN;
        }
        if (name.contains("Маслак")) {
            return AccessPoint.MASLAK;
        }
        if (name.contains("Вдовиченко")) {
            return AccessPoint.VDOVICHENKO;
        }
        if (name.contains("пролет 5 цех 12")) {
            return AccessPoint.PROLET5_CEH12;
        }
        return null;
    }
}
