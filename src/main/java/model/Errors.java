package model;

import lombok.Data;

@Data
public class Errors {
    private Employee employee;
    private StartEndTime startEndTime;

    public Errors(Employee employee, StartEndTime startEndTime) {
        this.employee = employee;
        this.startEndTime = startEndTime;
    }
}
